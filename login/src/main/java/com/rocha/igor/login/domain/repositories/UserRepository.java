package com.rocha.igor.login.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rocha.igor.login.domain.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
