package com.rocha.igor.login.services;

import org.springframework.stereotype.Service;

import com.rocha.igor.login.domain.entities.User;
import com.rocha.igor.login.domain.repositories.UserRepository;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository repository;

    @Transactional
    public User login(User user) {
        return repository.save(user);
    }

}
